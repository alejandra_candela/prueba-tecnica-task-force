<?php  include('./../layout/header.php'); ?>
<?php  include('./../layout/volver.php'); ?>

<div class="ejercicio-7">
  <h1>Ejercicio 7</h1>

  <p>Cuadricula de N x N con valores aleatorios por cada celda.</p>

  <div class="tabla">
  <?php
    // Verifica que exista HOST/Ejercicio_7/?numero=? de lo contrario usa 3 por defecto
    $numero = (isset($_REQUEST) && !empty($_REQUEST["numero"]))? $_REQUEST["numero"] : 3;

    $tabla= '<table border="1" class="nxn_tabla">';

    for ($i=0; $i < $numero; $i++) {
      $tabla.= '<tr>';

      for ($j=0; $j < $numero; $j++) {
        $min = ($numero / 3);
        $max = pow($numero, 2);
        $numeroAleatorio = rand($min, $max);

        $tabla.= '<td>';
        $tabla.= $numeroAleatorio;
        $tabla.= '</td>';
      }

      $tabla.= '</tr>';
    }

    $tabla .= '</table>';

    echo $tabla;
  ?>
  </div>

  <p>Si desea generar otra cuadricula diferente puede cambiar el <br> valor de la url <b>URL?numero=10</b> luego presione enter y la cuadricula <br> cambiara automaticamente</p>

</div>

<?php  include('./../layout/footer.php'); ?>
