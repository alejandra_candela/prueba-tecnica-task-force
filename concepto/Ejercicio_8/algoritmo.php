<?php

extract($_POST);

function procesar_texto($numeroVeces, $textoRepetitivo) {
    $mostrarTexto = '<table border="1" class="ejercicio-8-tabla">';
    $mostrarTexto.= '<tr>';
    $mostrarTexto.= '<td>#</td>';
    $mostrarTexto.= '<td>Texto</td>';
    $mostrarTexto.= '</tr>';

    for ($i=1; $i <= $numeroVeces ; $i++) {
      $mostrarTexto.= '<tr>';
      $mostrarTexto.= '<td>'. $i .'</td>';
      $mostrarTexto.= '<td>'. $textoRepetitivo .'</td>';
      $mostrarTexto.= '</tr>';
    }

    $mostrarTexto.= '</table>';

    return $mostrarTexto;
}

if (isset($numero) && !empty($numero) && is_numeric($numero)) {

    if (isset($texto) && !empty($texto)) {
        http_response_code(200);
        die(procesar_texto($numero, $texto));
    } else {
        http_response_code(400);
        die('No fue posible procesar el texto, texto vacio');
    }

} else {
    http_response_code(400);
    die('No fue posible procesar el texto, numero vacio o con formato incorrecto');
}

?>