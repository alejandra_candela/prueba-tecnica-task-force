<?php  include('./../layout/header.php'); ?>
<?php  include('./../layout/volver.php'); ?>

<script type="text/javascript" src="assets/js/ejercicio_8.js"></script>

<div class="ejercicio-8">
    <h1>Ejercicio 8</h1>

    <p>Imprimir N veces el texto ingresado desde el lado del servidor, por medio <br> de un llamado Ajax</p>
    <br>
    <br>

    <div class="tabla">

        <table>
            <tr>
                <td>
                <label>Ingrese el texto que desea mostrar</label>
                </td>
                <td><input type="text" name="texto" id="texto"></td>
            </tr>
            <tr>
                <td>
                <label>Ingrese el numero de veces que desea mostrar el texto</label>
                </td>
                <td><input type="number" name="numero" id="numero"></td>
            </tr>
            <tr>
                <td colspan="2">
                    <input type="button" name="enviar" id="enviar" value="enviar" onclick="peticionAjax()" class="button">
                </td>
            </tr>
        </table>

    </div>

    <div id="notificacion" class="notificacion">
        <div id="tipo-notificacion"></div>
        <div id="mensaje-notificacion"></div>
    </div>


    <div id="tabla-contenido" class="tabla-contenido"></div>

    <br>
    <br>

</div>

<?php  include('./../layout/footer.php'); ?>
