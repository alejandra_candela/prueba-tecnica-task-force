<?php  include('./../layout/header.php'); ?>
<?php  include('./../layout/volver.php'); ?>
<?php  $segundos = (isset($_GET['segundos']) && !empty($_GET['segundos']) && is_numeric($_GET['segundos']))? $_GET['segundos'] : 60 ; ?>


<div class="ejercicio-9">
    <h1>Ejercicio 9</h1>

    <p>Usar la clase depoprtista para definir si esta cansado o no dependiendo de la cantidad de segundos recorridos</p>
    <br>
    <br>

    <div class="tabla">

    <?php
        class deportista {
            function estascansado($seg) {
                if($seg >= 90) {
                    return true;
                } else {
                    return false;
                }
            }
        }

        $a = new deportista();
        $a = $a->estascansado($segundos);

        echo "<div class='deportista'><b>El deportista " . ($a ? "si esta cansado" : "no esta cansado") . "</b></div>";
    ?>
    <br>
    <br>
    <br>

    </div>

    <p>Si desea generar verificar con otro numero de segundos diferentes cambie el <br> valor de la url <b>URL?segundos=120</b> y presione enter y el estado sera verificado nuevamente</p>

</div>

<?php  include('./../layout/footer.php'); ?>