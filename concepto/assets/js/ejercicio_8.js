function peticionAjax() {
    var texto = $("#texto").val();
    var numero = $("#numero").val();

    $.ajax({
        url: "Ejercicio_8/algoritmo.php",
        method: 'POST',
        data: {
            texto: texto,
            numero: numero
        },
        success: function(result) {
            $("#tabla-contenido").html(result);
            notificacion('exito', "Peticion ajax completada");
        },
        error: function(result) {
            notificacion('error', result.responseText);
        }
    });
}

function notificacion(tipo, mensaje) {
    $("#tipo-notificacion").text(tipo);
    $("#mensaje-notificacion").text(mensaje);
    $("#notificacion").addClass(tipo).fadeIn();

    setTimeout(function() {
        $("#tipo-notificacion").text('');
        $("#mensaje-notificacion").text('');
        $("#notificacion").removeClass(tipo).fadeOut();
    }, 3000);
}