<?php  include('./layout/header.php'); ?>

<div class="contenedor-home">
    <h1>Prueba Tecnica</h1>

    <p>A continuacion podra ver el resultado de los ejercicios 7,8,9.</p>

    <ul>
        <li>
            <a href="./Ejercicio_7/?numero=6">Ejercicio 7</a>
        </li>
        <li>
            <a href="./Ejercicio_8/">Ejercicio 8</a>
        </li>
        <li>
            <a href="./Ejercicio_9/?segundos=60">Ejercicio 9</a>
        </li>
    </ul>

    <div class="pesentado-por">
        <div><b>Presentado por:</b></div>
        <div>Yuly Alejandra candela Heredia</div>
    </div>
</div>

<?php  include('./layout/footer.php'); ?>